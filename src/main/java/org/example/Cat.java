package org.example;

import java.lang.reflect.Field;

public class Cat {
    Animal animal = new Animal("Elephant", "Dog", "Mouse");
    Class<? extends Animal> animalClass = animal.getClass();
    Field[] declaredFields = animalClass.getDeclaredFields();


    public void chaker() throws NoSuchFieldException, IllegalAccessException {
        for (Field field : declaredFields) {
            System.out.println(field);
            Field declaredField = animalClass.getDeclaredField("dog");
            if (field.equals(declaredField)) {
                System.out.println("Before change:" + declaredField.get(animal));
                declaredField.setAccessible(true);
                declaredField.set(animal, "Happy dog");
                System.out.println("After change:" + declaredField.get(animal));
            }
            Field field1 = animalClass.getField("elephant");
            if (field.equals(field1)) {
                System.out.println("Before change:" + field1.get(animal));
                field1.setAccessible(true);
                field1.set(animal, "Happy elephant");
                System.out.println("After change:" + field1.get(animal));

            }
            Field field2 = animalClass.getDeclaredField("mouse");
            if (field.equals(field2)){
                field2.setAccessible(true);
                System.out.println("Before change:" + field2.get(animal));
                field2.set(animal, "Happy mouse");
                System.out.println("After change:" + field2.get(animal));
            }
        }
    }
}