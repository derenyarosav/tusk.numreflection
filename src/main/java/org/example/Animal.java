package org.example;

public class Animal {
    public String elephant;
    protected String dog;
    private String mouse;
    public Animal (String elephant , String dog, String mouse){
        this.elephant = elephant;
        this.dog = dog;
        this.mouse = mouse;
    }

    public void setMouse(String mouse) {
        this.mouse = mouse;
    }

    public String getMouse() {
        return mouse;
    }

    public void setElephant(String elephant) {
        this.elephant = elephant;
    }

    public String getElephant() {
        return elephant;
    }

    public void setDog(String dog) {
        this.dog = dog;
    }

    public String getDog() {
        return dog;
    }

    @Override
    public String toString() {
        return "Animal{" +
                "elephant='" + elephant + '\'' +
                ", dog='" + dog + '\'' +
                ", mouse='" + mouse + '\'' +
                '}';
    }
}
